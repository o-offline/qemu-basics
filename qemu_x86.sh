#!/bin/bash
# 
# start me:  ubuntu> bash <(curl -s https://gitlab.com/o-offline/qemu-basics/-/raw/main/qemu_x86.sh?ref_type=heads)
#

export MY_PRJ_QEMU=$PWD/qemu/

mkdir -p $MY_PRJ_QEMU && cd $MY_PRJ_QEMU

printf "\n******* installing prerequisits\n"
sudo apt update && sudo apt upgrade -y && sudo apt install qemu-system-x86 build-essential flex bison libncurses-dev libelf-dev libssl-dev -y


printf "\n******* compiling linux\n"
git clone --branch linux-6.6.y --depth 1 https://gitlab.com/linux-kernel/stable.git linux && \
(cd linux && make defconfig && ./scripts/config --enable  CONFIG_BLK_DEV_RAM && make olddefconfig && make -j)


printf "\n******* compiling busybox\n"
git clone --branch 1_36_stable --depth 1 https://git.busybox.net/busybox.git busybox && \
#(cd busybox && make defconfig && \
(cd busybox && make allnoconfig  && \
 ../linux/scripts/config --enable  CONFIG_STATIC && \
 ../linux/scripts/config --enable  CONFIG_CAT && \
 ../linux/scripts/config --enable  CONFIG_CLEAR && \
 ../linux/scripts/config --enable  CONFIG_ECHO && \
 ../linux/scripts/config --enable  CONFIG_LS && \
 ../linux/scripts/config --enable  CONFIG_FEATURE_LS_COLOR_IS_DEFAULT && \
 ../linux/scripts/config --enable  CONFIG_MKDIR && \
 ../linux/scripts/config --enable  CONFIG_MOUNT && \
 ../linux/scripts/config --enable  CONFIG_SLEEP && \
 ../linux/scripts/config --enable  CONFIG_FEATURE_TAB_COMPLETION && \
 ../linux/scripts/config --enable  CONFIG_TOP && \
 ../linux/scripts/config --enable  CONFIG_FEATURE_TOP_INTERACTIVE && \
 ../linux/scripts/config --enable  CONFIG_FEATURE_TOP_SMP_CPU && \
 ../linux/scripts/config --enable  CONFIG_FEATURE_TOPMEM && \
 ../linux/scripts/config --enable  CONFIG_VI && \
 ../linux/scripts/config --set-val CONFIG_FEATURE_VI_MAX_LEN 4096 && \
 ../linux/scripts/config --disable CONFIG_ASH_JOB_CONTROL && \
  make -j && make install)

cat > busybox/_install/init << INIT
#!/bin/sh

mkdir -p /proc /sys
mount -t proc none /proc
mount -t sysfs none /sys
# I don't like this in productive code, but it ensures these traces come after the kernel traces
sleep 1
cat /proc/version
echo ""
echo "Hello World, greetinX from Olaf"
exec /bin/sh
INIT


printf "\n******* creating initramfs\n"
(cd busybox/_install && chmod +x init && find . -print0 | cpio --null -ov --format=newc | gzip -9 > $MY_PRJ_QEMU/initramfs.cpio.gz)


printf "\n******* starting qemu\n"
exec qemu-system-x86_64 -kernel linux/arch/x86_64/boot/bzImage -initrd initramfs.cpio.gz -append "console=ttyS0" -nographic -smp 4
